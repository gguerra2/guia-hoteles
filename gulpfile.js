'use strict'

const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const browserSync = require('browser-sync').create();
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

//compile sccs into css
function style(){
    //1. where is my scss file
    return gulp.src('./css/*.scss')
        //2. pass that file through sass compiler
        .pipe(sass().on('error', sass.logError))
        //3. where do I save the compiled CSS?
        .pipe(gulp.dest('./css'))
        //4. stream changes to all browser
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: './'

        }
    });
    gulp.watch('./css/*.scss', style);
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch('./js/*.js').on('change', browserSync.reload);
    gulp.watch('./img/*.{png, jpg, gif}').on('change', browserSync.reload);


}

exports.style = style;
exports.watch = watch;